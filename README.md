# Training CMS
Install dependencies:

```bash
npm install
```

Copy `example.env` to a new file named `.env` and update it with correct values.

Create redis server:

```bash
docker-compose up
```

Run the development server:

```bash
npm run dev
```

Run the worker server:

```bash
npm run worker 
```

