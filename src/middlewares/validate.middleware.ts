import { Request, Response, NextFunction } from 'express';
import Joi from 'joi';
import ApiError from '../utils/api.error.util';

export const validateRequest = (schema: Joi.ObjectSchema) => {
  return (req: Request, res: Response, next: NextFunction) => {
    const { error } = schema.validate(req.body, { abortEarly: false });
    if (error) {
      const errorMessage = error.details.map(detail => detail.message).join(', ');
      throw new ApiError(errorMessage, 400);
    }
    next();
  };
};
