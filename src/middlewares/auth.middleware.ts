import { Request, Response, NextFunction } from 'express';
import jwt from 'jsonwebtoken';
import { AuthChecker } from 'type-graphql';
import globalConfig from '../common/config/global.config';

interface JwtPayload {
  userId: string;
}

const authMiddleware = (req: Request, res: Response, next: NextFunction) => {
  const token = req.header('Authorization')?.split(' ')[1];
  if (!token) return res.status(401).json({ error: 'Access denied, no token provided' });

  try {
    const decoded = jwt.verify(token, globalConfig.auth.jwtSecret!) as JwtPayload;
    req.params.userId = decoded.userId;
    next();
  } catch (ex) {
    res.status(400).json({ error: 'Invalid token' });
  }
};

export default authMiddleware;

export interface MyContext {
  req: Request;
  res: Response;
}

// Define the AuthChecker function
export const authChecker: AuthChecker<MyContext> = ({ context }, roles) => {
  const authHeader = context.req.headers['authorization'];
  if (!authHeader) {
    throw new Error('Access denied, no token provided');
  }

  const token = authHeader.split(' ')[1];
  if (!token) {
    throw new Error('Access denied, no token provided');
  }

  try {
    const decoded = jwt.verify(token, globalConfig.auth.jwtSecret!) as jwt.JwtPayload;
    context.req.params.userId = decoded.userId;
    return true; // or perform role checks and return based on the roles argument
  } catch (ex) {
    throw new Error('Invalid token');
  }
};
