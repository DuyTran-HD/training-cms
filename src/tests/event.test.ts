import instanceRedis from "../dbs/init.redis";

async function checkEditable(
  eventId: string,
  userId: string
): Promise<boolean> {
  const key = `event:${eventId}:editing`;
  const isSet = await redisClient.setNX(key, userId);
  if (isSet) {
    await redisClient.expire(key, EDIT_TIMEOUT);
    return true;
  }
  const editingUser = await redisClient.get(key);
  return editingUser === userId;
}

jest.mock("../dbs/init.redis", () => ({
  getClient: jest.fn(),
}));

const redisClient = {
  setNX: jest.fn(),
  expire: jest.fn(),
  get: jest.fn(),
  quit: jest.fn(),
};

(instanceRedis.getClient as jest.Mock).mockReturnValue(redisClient);

const EDIT_TIMEOUT = 300;

describe("checkEditable", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("should handle race conditions correctly", async () => {
    const eventId = "testEvent";
    const userId1 = "user1";
    const userId2 = "user2";

    redisClient.setNX.mockImplementationOnce(async (key, userId) => {
      if (userId === "user1") return true;
      return false;
    });

    redisClient.get.mockImplementation(async (key) => "user1");

    const promises = [
      checkEditable(eventId, userId1),
      checkEditable(eventId, userId2),
    ];

    const results = await Promise.all(promises);

    console.log("Results:", results);

    expect(results).toEqual([true, false]);

    redisClient.quit();
  });
});


