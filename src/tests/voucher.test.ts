import mongoose from 'mongoose';
import { VoucherModel } from '../models/voucher.model';
import VoucherService  from '../services/voucher.service';
import EventService from '../services/event.service';
import globalConfig from '../common/config/global.config';
const voucherService = VoucherService.getInstance()
const eventService = EventService.getInstance()
describe('Voucher Service', () => {
  beforeAll(async () => {
    const connectString: string = globalConfig.db.mongo.url; 
    await mongoose.connect(connectString);
  });

  afterAll(async () => {
    await mongoose.connection.close();
  });
  const delay = (ms: number) => new Promise((resolve) => setTimeout(resolve, ms));

  it('should create an event and issue vouchers up to the maxQuantity', async () => {
    const eventData = { name: 'Test Event', maxQuantity: 3 };
    const event = await eventService.createEvent(eventData);

    const createVoucherWithRetries = async (retries: number, delayMs: number) => {
      for (let i = 0; i < retries; i++) {
        const createVoucherPromises = Array.from({ length: 6 }).map(() =>
          voucherService.createVoucher(String(event._id))
        );

        await Promise.allSettled(createVoucherPromises);
        await delay(delayMs);
      }
    };

    await createVoucherWithRetries(3, 10);

    const vouchers = await VoucherModel.find({ eventId: event._id });
    expect(vouchers.length).toBe(3);
  });
});
