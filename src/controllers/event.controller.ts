import { Request, Response, NextFunction } from 'express';
import EventService from '../services/event.service';
import { sendSuccessResponse } from '../utils/response.util';
import { CreateEventDTO, UpdateEventDTO } from '../dtos/event.dto';
class EventController {
  private static instance: EventController;
  private eventService: EventService;

  private constructor() {
    this.eventService = EventService.getInstance();
  }

  public static getInstance(): EventController {
    if (!EventController.instance) {
      EventController.instance = new EventController();
    }
    return EventController.instance;
  }

  createEvent = async (req: Request, res: Response, next: NextFunction) => {
    const eventData: CreateEventDTO = req.body;
    try {
      const event = await this.eventService.createEvent(eventData);
      sendSuccessResponse(res, event, 201);
    } catch (err) {
      next(err);
    }
  }

  getEvent = async (req: Request, res: Response, next: NextFunction) => {
    const { eventId } = req.params;
    try {
      const event = await this.eventService.getEvent(eventId);
      sendSuccessResponse(res, event);
    } catch (err) {
      next(err);
    }
  }

  updateEvent = async (req: Request, res: Response, next: NextFunction) => {
    const { eventId } = req.params;
    const updateData: UpdateEventDTO = req.body;
    try {
      const event = await this.eventService.updateEvent(eventId, updateData);
      sendSuccessResponse(res, event);
    } catch (err) {
      next(err);
    }
  }

  deleteEvent = async (req: Request, res: Response, next: NextFunction) => {
    const { eventId } = req.params;
    try {
      const event = await this.eventService.deleteEvent(eventId);
      sendSuccessResponse(res, event);
    } catch (err) {
      next(err);
    }
  }

  listEvents = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const events = await this.eventService.listEvents();
      sendSuccessResponse(res, events);
    } catch (err) {
      next(err);
    }
  }

  checkEditable = async (req: Request, res: Response, next: NextFunction) => {
    const { eventId } = req.params;
    const { userId } = req.body;
    try {
      const isEditable = await this.eventService.checkEditable(eventId, userId);
      if (isEditable) {
        sendSuccessResponse(res, { message: 'Event is editable' }, 200);
      } else {
        res.status(409).json({ message: 'Event is being edited by another user' });
      }
    } catch (err) {
      next(err);
    }
  }

  releaseEdit = async (req: Request, res: Response, next: NextFunction) => {
    const { eventId } = req.params;
    const { userId } = req.body;
    try {
      const isReleased = await this.eventService.releaseEdit(eventId, userId);
      if (isReleased) {
        sendSuccessResponse(res, { message: 'Edit released' }, 200);
      } else {
        res.status(409).json({ message: 'You are not the editing user' });
      }
    } catch (err) {
      next(err);
    }
  }

  maintainEdit = async (req: Request, res: Response, next: NextFunction) => {
    const { eventId } = req.params;
    const { userId } = req.body;
    try {
      const isMaintained = await this.eventService.maintainEdit(eventId, userId);
      if (isMaintained) {
        sendSuccessResponse(res, { message: 'Edit maintained' }, 200);
      } else {
        res.status(409).json({ message: 'You are not the editing user' });
      }
    } catch (err) {
      next(err);
    }
  }

}

const eventController = EventController.getInstance();
export default eventController;
