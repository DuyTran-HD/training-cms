import { Request, Response, NextFunction } from 'express';
import { sendSuccessResponse } from '../utils/response.util';
import { LoginDto, RegisterDto } from '../dtos/user.dto';
import UserService from '../services/user.service';

class UserController {
  private static instance: UserController;
  private userService: UserService;

  private constructor() {
    this.userService = UserService.getInstance();
  }

  public static getInstance(): UserController {
    if (!UserController.instance) {
      UserController.instance = new UserController();
    }
    return UserController.instance;
  }

  register = async (req: Request, res: Response, next: NextFunction) => {
    const registerDto: RegisterDto = req.body;
    console.log(registerDto);
    
    try {
      const user = await this.userService.register(registerDto);
      sendSuccessResponse(res, user, 201);
    } catch (err) {
      next(err);
    }
  }

  login = async (req: Request, res: Response, next: NextFunction) => {
    const loginDto: LoginDto = req.body;
    try {
      const token = await this.userService.login(loginDto);
      sendSuccessResponse(res, { token });
    } catch (err) {
      next(err);
    }
  }

  getUser = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const user = await this.userService.getUserById(req.params.userId);
      sendSuccessResponse(res, user);
    } catch (err) {
      next(err);
    }
  }
}

const userController = UserController.getInstance();
export default userController;
