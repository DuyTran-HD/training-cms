import { Request, Response, NextFunction } from 'express';
import { sendSuccessResponse } from '../utils/response.util';
import VoucherService from '../services/voucher.service';

class VoucherController {
  private static instance: VoucherController;
  private voucherService: VoucherService;

  private constructor() {
    this.voucherService = VoucherService.getInstance();
  }

  public static getInstance(): VoucherController {
    if (!VoucherController.instance) {
      VoucherController.instance = new VoucherController();
    }
    return VoucherController.instance;
  }

  createVoucher = async (req: Request, res: Response, next: NextFunction) => {
    const { eventId } = req.body;
    try {
      const voucher = await this.voucherService.createVoucher(eventId);
      sendSuccessResponse(res, voucher, 201);
    } catch (err) {
      next(err);
    }
  }

  getVouchers = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const vouchers = await this.voucherService.getVouchers();
      sendSuccessResponse(res, vouchers);
    } catch (err) {
      next(err);
    }
  }
}

const voucherController = VoucherController.getInstance();
export default voucherController;
