// src/graphql/types/eventType.ts
import { Field, ID, ObjectType } from 'type-graphql';

@ObjectType()
export class Event {
  @Field(() => ID)
  id!: string;

  @Field()
  name!: string;

  @Field()
  maxQuantity!: number;

  @Field()
  issuedQuantity!: number;
}
