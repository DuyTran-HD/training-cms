import { Field, ID, ObjectType } from 'type-graphql';

@ObjectType()
export class User {
    @Field()
    id!: string;

    @Field()
    name!: string;

    @Field()
    email!: string;

}

@ObjectType()
export class Login {
  @Field()
  token!: string;
}