import { buildSchema } from "type-graphql";
import "reflect-metadata";
import { EventResolver } from "./resolvers/event.resolver";
import { graphqlHTTP } from "express-graphql";
import express, { Express, Request, Response } from "express";
import { AuthResolver } from "./resolvers/auth.resolver";
import { authChecker } from "../middlewares/auth.middleware";
class GraphQLSetup {
  private static instance: GraphQLSetup;

  private constructor() {}

  private async createSchema() {
    return await buildSchema({
      resolvers: [EventResolver, AuthResolver],
      authChecker: authChecker,
    });
  }

  public static getInstance(): GraphQLSetup {
    if (!GraphQLSetup.instance) {
      GraphQLSetup.instance = new GraphQLSetup();
    }
    return GraphQLSetup.instance;
  }

  public async setup(app: Express): Promise<void> {
    const schema = await this.createSchema();
    app.use('/graphql', graphqlHTTP((req, res) => ({
      schema: schema,
      context: { req, res },
      graphiql: true,
    })));
    app.use("/playground", (req, res) => {
      res.send(`
        <html>
          <head>
            <title>GraphQL Playground</title>
          </head>
          <body>
            <script src="https://cdn.jsdelivr.net/npm/graphql-playground-react@1.7.26/build/static/js/middleware.js"></script>
            <script>window.addEventListener('load', function(event) { GraphQLPlayground.init(document.getElementById('root'), { endpoint: '/graphql' }) })</script>
            <div id="root"></div>
          </body>
        </html>
      `);
    });
    console.log("GraphQL setup completed");
  }
}

const instanceGraphQL = GraphQLSetup.getInstance();
export default instanceGraphQL;
