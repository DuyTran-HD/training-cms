// src/graphql/resolvers/eventResolver.ts
import {
  Resolver,
  Query,
  Mutation,
  Arg,
  Field,
  ID,
  Args,
  ArgsType,
  Authorized
} from "type-graphql";
import { EventModel, IEvent } from "../../models/event.model";
import { Event } from "../type/event.type";
import { IsNumber, IsString, Min } from "class-validator";

@ArgsType()
export class EventInput {
  @Field({ name: "name" })
  @IsString()
  name!: string;

  @Field({ name: "maxQuantity" })
  @IsNumber()
  @Min(0)
  maxQuantity!: number;

  @Field({ name: "issuedQuantity" })
  @IsNumber()
  @Min(0)
  issuedQuantity!: number;
}

@Resolver()
export class EventResolver {

  @Authorized()
  @Query(() => Event)
  async getEvent(@Arg("id", () => ID) id: string): Promise<Event> {
    const data = await EventModel.findOne({ _id: id });
    if (!data) {
      throw new Error("Event not found");
    }
    const event: Event = {
      id: String(data._id),
      name: data.name,
      maxQuantity: data.maxQuantity,
      issuedQuantity: data.issuedQuantity,
    };
    console.log(event);

    return event;
  }

  @Authorized()
  @Query(() => [Event])
  async getEvents(): Promise<IEvent[]> {
    return await EventModel.find().exec();
  }

  @Authorized()
  @Mutation(() => Event)
  async createEvent(
    @Args() { name, maxQuantity, issuedQuantity }: EventInput
  ): Promise<IEvent> {
    const event = new EventModel({ name, maxQuantity, issuedQuantity });

    event.save();
    return event;
  }

  @Authorized()
  @Mutation(() => Event, { nullable: true })
  async updateEvent(
    @Arg("id", () => ID) id: string,
    @Args() { name, maxQuantity, issuedQuantity }: EventInput
  ): Promise<IEvent | null> {
    return await EventModel.findByIdAndUpdate(id, { name, maxQuantity, issuedQuantity }, { new: true }).exec();
  }
  
  @Mutation(() => String)
  async deleteEvent(@Arg('id', () => ID) id: string): Promise<string> {
    await EventModel.findByIdAndDelete(id).exec();
    return `Event with id ${id} deleted`;
  }
}
