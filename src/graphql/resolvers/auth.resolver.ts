import { Resolver, Field, ArgsType, Mutation, Args } from "type-graphql";
import { IsString } from "class-validator";
import User, { IUser } from "../../models/user.model";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import ApiError from "../../utils/api.error.util";
import { Login } from "../type/user.type";
import globalConfig from "../../common/config/global.config";
@ArgsType()
export class AuthPayLoad {
  @Field({ name: "email" })
  @IsString()
  email!: string;

  @Field({ name: "password" })
  @IsString()
  password!: string;
}

@ArgsType()
export class RegisterPayLoad {
  @Field({ name: "name" })
  @IsString()
  name!: string;

  @Field({ name: "name" })
  @IsString()
  email!: string;

  @Field({ name: "password" })
  @IsString()
  password!: string;
}

@Resolver()
export class AuthResolver {
  @Mutation(() => Login)
  async login(
    @Args() { email, password }: AuthPayLoad
  ): Promise<Login> {
    const user = await User.findOne({ email });
    if (user && (await bcrypt.compare(password, user.password))) {
      const token = jwt.sign({ userId: user._id }, globalConfig.auth.jwtSecret!, {
        expiresIn: "1h",
      });
      return { token };
    }
    return { token: "" };
  }

  @Mutation(() => String)
  async register(
    @Args() { name, email, password }: RegisterPayLoad
  ): Promise<IUser> {
    const existingUser = await User.findOne({ email });
    if (existingUser) {
      throw new ApiError('User already exists', 409);
    }
    const hashedPassword = await bcrypt.hash(password, 10);
    const user = new User({ ...{ name, email }, password: hashedPassword });
    user.save();
    return user;
  }
 
}
