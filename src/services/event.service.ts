import { CreateEventDTO, UpdateEventDTO } from "../dtos/event.dto";
import { IEvent, EventModel } from "../models/event.model";
import ApiError from "../utils/api.error.util";
import instanceRedis from "../dbs/init.redis";

const redisClient = instanceRedis.getClient();
const EDIT_TIMEOUT = 300;
class EventService {
  private static instance: EventService;

  private constructor() {}

  public static getInstance(): EventService {
    if (!EventService.instance) {
      EventService.instance = new EventService();
    }
    return EventService.instance;
  }

  async createEvent(eventData: CreateEventDTO): Promise<IEvent> {
    const event = new EventModel(eventData);
    await event.save();
    return event;
  }

  async getEvent(eventId: string): Promise<IEvent | null> {
    const event = await EventModel.findOne({ _id: eventId });
    if (!event) {
      throw new ApiError("Event not found", 404);
    }
    return event;
  }

  async updateEvent(
    eventId: string,
    updateData: UpdateEventDTO
  ): Promise<IEvent | null> {
    const event = await EventModel.findOneAndUpdate(
      { _id: eventId },
      updateData,
      {
        new: true,
      }
    );
    if (!event) {
      throw new ApiError("Event not found", 404);
    }
    return event;
  }

  async deleteEvent(eventId: string): Promise<IEvent | null> {
    const event = await EventModel.findOneAndDelete({ _id: eventId });
    if (!event) {
      throw new ApiError("Event not found", 404);
    }
    return event;
  }

  async listEvents(): Promise<IEvent[]> {
    return EventModel.find();
  }

  async checkEditable(eventId: string, userId: string): Promise<boolean> {
    const key = `event:${eventId}:editing`;
    const isSet = await redisClient.setNX(key, userId);
    if (isSet) {
      await redisClient.expire(key, EDIT_TIMEOUT);
      return true;
    }
    const editingUser = await redisClient.get(key);
    return editingUser === userId;
  }
  async releaseEdit(eventId: string, userId: string): Promise<boolean> {
    const key = `event:${eventId}:editing`;

    try {
      const editingUser = await redisClient.get(key);
      if (editingUser === userId) {
        await redisClient.del(key);
        return true;
      } else {
        return false;
      }
    } catch (error) {
      console.error("Error releasing edit:", error);
      return false;
    }
  }

  async maintainEdit(eventId: string, userId: string): Promise<boolean> {
    const editingUser = await redisClient.get(`event:${eventId}:editing`);
    if (editingUser === userId) {
      await redisClient.expire(`event:${eventId}:editing`, EDIT_TIMEOUT);
      return true;
    }
    return false;
  }
}

export default EventService;
