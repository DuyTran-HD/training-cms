import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import { LoginDto, RegisterDto } from '../dtos/user.dto';
import User, { IUser } from '../models/user.model';
import ApiError from '../utils/api.error.util';
import globalConfig from '../common/config/global.config';

class UserService {
  private static instance: UserService;

  private constructor() {}

  public static getInstance(): UserService {
    if (!UserService.instance) {
      UserService.instance = new UserService();
    }
    return UserService.instance;
  }

  async register(registerDto: RegisterDto): Promise<IUser> {
    const existingUser = await User.findOne({ email: registerDto.email });
    if (existingUser) {
      throw new ApiError('User already exists', 409);
    }
    const hashedPassword = await bcrypt.hash(registerDto.password, 10);
    const user = new User({ ...registerDto, password: hashedPassword });
    user.save();
    return user
  }

  async login(loginDto: LoginDto): Promise<string | null> {
    const user = await User.findOne({ email: loginDto.email });
    if (user && await bcrypt.compare(loginDto.password, user.password)) {
      return jwt.sign({ userId: user._id }, globalConfig.auth.jwtSecret!, { expiresIn: '1h' });
    }
    return null;
  }

  async getUserById(userId: string): Promise<IUser | null> {
    return User.findById(userId);
  }
}

export default UserService;
