import mongoose from 'mongoose';
import { EventModel } from '../models/event.model';
import { IVoucher, VoucherModel } from '../models/voucher.model';
import ApiError from '../utils/api.error.util';
import eventEmitter from '../events/event-emitter';

class VoucherService {
  private static instance: VoucherService;

  private constructor() {}

  public static getInstance(): VoucherService {
    if (!VoucherService.instance) {
      VoucherService.instance = new VoucherService();
    }
    return VoucherService.instance;
  }

  async  createVoucher(eventId: string): Promise<IVoucher> {
    const session = await mongoose.startSession();
    session.startTransaction();
  
    try {
     
      const event = await EventModel.findOne({ _id: eventId }).session(session).exec();
      if (!event) {
        throw new ApiError('Event not found', 404);
      }
  
      // Check if the event has reached the maximum quantity of vouchers
      if (event.issuedQuantity >= event.maxQuantity) {
        throw new ApiError('Maximum quantity of vouchers issued', 456);
      }
  
      // Increment issuedQuantity atomically
      const updatedEvent = await EventModel.findOneAndUpdate(
        { _id: eventId, issuedQuantity: { $lt: event.maxQuantity } },
        { $inc: { issuedQuantity: 1 } },
        { session, new: true }
      ).exec();
  
      if (!updatedEvent) {
        throw new ApiError('Failed to increment issuedQuantity or maximum quantity exceeded', 456);
      }
  
      const voucherCode = `VOUCHER-${Date.now()}`;
  
      const newVoucher = new VoucherModel({
        voucherId: new mongoose.Types.ObjectId().toString(),
        eventId,
        voucherCode,
        issuedAt: new Date(),
      });
  
      await newVoucher.save({ session });
  
      await session.commitTransaction();
      session.endSession();
  
      eventEmitter.emit('addToQueue', { email: "test@gmail.com", newVoucher: voucherCode });
      return newVoucher;
    } catch (error) {
      await session.abortTransaction();
      session.endSession();
      throw error;
    }
  }
  async getVouchers(): Promise<IVoucher[]> {
    return VoucherModel.find().populate('eventId', 'name');
  }
}

export default VoucherService;
