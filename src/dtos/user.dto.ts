import Joi from 'joi';

export interface RegisterDto {
  name: string;
  email: string;
  password: string;
}

export const registerSchema = Joi.object<RegisterDto>({
  name: Joi.string().required(),
  email: Joi.string().email().required(),
  password: Joi.string().min(6).required()
});

export interface LoginDto {
  email: string;
  password: string;
}

export const loginSchema = Joi.object<LoginDto>({
  email: Joi.string().email().required(),
  password: Joi.string().min(6).required()
});