import Joi from 'joi';
import mongoose from 'mongoose';

export interface CreateVoucherDTO {
  eventId: mongoose.Types.ObjectId;
  issuedAt?: Date;
}

export const createVoucherSchema = Joi.object<CreateVoucherDTO>({
  eventId: Joi.string().custom((value, helpers) => {
    if (!mongoose.Types.ObjectId.isValid(value)) {
      return helpers.error('any.invalid');
    }
    return value;
  }, 'ObjectId validation').required(),
  issuedAt: Joi.date().optional()
});
