import Joi from 'joi';

export interface CreateEventDTO {
  name: string;
  maxQuantity: number;
}

export const createEventSchema = Joi.object<CreateEventDTO>({
  name: Joi.string().required(),
  maxQuantity: Joi.number().integer().min(0).required(),
});

export interface UpdateEventDTO {
  name: string;
  maxQuantity: number;
}
export const updateEventSchema = Joi.object<UpdateEventDTO>({
  name: Joi.string().optional(),
  maxQuantity: Joi.number().integer().min(0).optional(),
})