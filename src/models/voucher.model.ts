import mongoose, { Document, Schema } from 'mongoose';

interface IVoucher extends Document {
  eventId: mongoose.Types.ObjectId;
  voucherCode: string;
  issuedAt: Date;
}

const VoucherSchema = new Schema<IVoucher>({
  eventId: { type: Schema.Types.ObjectId, ref: 'Event', required: true },
  voucherCode: { type: String, required: true },
  issuedAt: { type: Date, default: Date.now }
});

const VoucherModel = mongoose.model<IVoucher>("Voucher", VoucherSchema);

export { VoucherModel, IVoucher };