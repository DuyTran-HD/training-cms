import mongoose, { Document, Schema } from 'mongoose';

 interface IEvent extends Document {
  name: string;
  maxQuantity: number;
  issuedQuantity: number;
}

const EventSchema = new Schema<IEvent>({
  name: { type: String, required: true },
  maxQuantity: { type: Number, required: true },
  issuedQuantity: { type: Number, required: true, default: 0 },
});

const EventModel = mongoose.model<IEvent>("Event", EventSchema);

export {EventModel,IEvent} ;