import express, { Express } from "express";
import UserRouter from "../routers/user.router";
import EventRouter from "../routers/event.router";
import VoucherRouter from "../routers/voucher.router";
import bodyParser from 'body-parser';
import instanceMongodb from "../dbs/init.database";
import { setupSwagger } from "../common/config/swagger.config";
import cors from 'cors';
import errorHandler from "../middlewares/error.middleware";
import instanceBullQueue from "../dbs/init.bull";
import instanceRedis from "../dbs/init.redis";
import  instanceGraphQL  from "../graphql/instance.graphql";
instanceMongodb;
instanceBullQueue;
instanceRedis;
const app: Express = express();
app.use(bodyParser.json());
app.use(cors());
app.use("/api/users", UserRouter);
app.use("/api", EventRouter);
app.use("/api", VoucherRouter);
instanceGraphQL.setup(app);


setupSwagger(app);
app.use(errorHandler);
export default app;
