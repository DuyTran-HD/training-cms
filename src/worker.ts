import worker from "./worker/worker";

require('dotenv').config();

const PORT =  3002;

worker.listen(PORT, () => {
  console.log(`Server is running on ${PORT}`);
});
