import * as dotenv from 'dotenv';
dotenv.config();

const globalConfig = {
  environment: process.env.NODE_ENV,
  port: Number(process.env.PORT) || 5000,
  db: {
    mongo:{
      url: process.env.MONGO_URL || 'mongodb://localhost:27017',
    },
    redis: {
      url: process.env.REDIS_URL || 'redis://localhost:6379',
    },
    postgres:{

    }
  },
  auth: {
    jwtSecret: process.env.JWT_SECRET || 'secret',
  },
};

export default globalConfig;
export type GlobalConfig = typeof globalConfig;
