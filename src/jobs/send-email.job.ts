import { Job } from 'bull';

const sendEmail = async (job: Job) => {
  const { eventId, voucherCode } = job.data;

  console.log(`Sending email for Event ID: ${eventId} with Voucher Code: ${voucherCode}`);

};

export default sendEmail;
