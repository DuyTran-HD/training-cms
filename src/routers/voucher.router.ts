import { Router } from 'express';
import authMiddleware from '../middlewares/auth.middleware';
import voucherController from '../controllers/voucher.controller';
import { validateRequest } from '../middlewares/validate.middleware';
import { createVoucherSchema } from '../dtos/voucher.dto';

const router = Router();

/**
 * @swagger
 * tags:
 *   name: Vouchers
 *   description: Voucher management
 */

/**
 * @swagger
 * /vouchers:
 *   post:
 *     summary: Create a new voucher
 *     tags: [Vouchers]
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             required:
 *               - eventId
 *             properties:
 *               eventId:
 *                 type: string
 *     responses:
 *       201:
 *         description: Voucher created successfully
 *       400:
 *         description: Invalid input
 *       401:
 *         description: Unauthorized
 *       456:
 *         description: Maximum quantity of vouchers issued
 *       500:
 *         description: Internal server error
 */
router.post('/vouchers', authMiddleware, validateRequest(createVoucherSchema), voucherController.createVoucher);

/**
 * @swagger
 * /vouchers:
 *   get:
 *     summary: Get all vouchers
 *     tags: [Vouchers]
 *     responses:
 *       200:
 *         description: List of vouchers retrieved successfully
 *       500:
 *         description: Internal server error
 */
router.get('/vouchers', voucherController.getVouchers);

export default router;
