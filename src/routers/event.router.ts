import { Router } from 'express';
import authMiddleware from '../middlewares/auth.middleware';
import eventController from '../controllers/event.controller';
import { validateRequest } from '../middlewares/validate.middleware';
import { createEventSchema, updateEventSchema } from '../dtos/event.dto';

const router = Router();

/**
 * @swagger
 * tags:
 *   name: Events
 *   description: Event management
 */

/**
 * @swagger
 * /events:
 *   post:
 *     summary: Create a new event
 *     tags: [Events]
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             required:
 *               - name
 *               - maxQuantity
 *             properties:
 *               name:
 *                 type: string
 *               maxQuantity:
 *                 type: number
 *     responses:
 *       201:
 *         description: Event created successfully
 *       400:
 *         description: Invalid input
 *       401:
 *         description: Unauthorized
 *       500:
 *         description: Internal server error
 */
router.post('/events', authMiddleware, validateRequest(createEventSchema), eventController.createEvent);

/**
 * @swagger
 * /events/{eventId}:
 *   get:
 *     summary: Get event by ID
 *     tags: [Events]
 *     parameters:
 *       - in: path
 *         name: eventId
 *         schema:
 *           type: string
 *         required: true
 *         description: The event ID
 *     responses:
 *       200:
 *         description: Event retrieved successfully
 *       404:
 *         description: Event not found
 *       500:
 *         description: Internal server error
 */
router.get('/events/:eventId', authMiddleware, eventController.getEvent);

/**
 * @swagger
 * /events/{eventId}:
 *   put:
 *     summary: Update event by ID
 *     tags: [Events]
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: eventId
 *         schema:
 *           type: string
 *         required: true
 *         description: The event ID
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *               maxQuantity:
 *                 type: number
 *     responses:
 *       200:
 *         description: Event updated successfully
 *       404:
 *         description: Event not found
 *       500:
 *         description: Internal server error
 */
router.put('/events/:eventId', authMiddleware, validateRequest(updateEventSchema), eventController.updateEvent);

/**
 * @swagger
 * /events/{eventId}:
 *   delete:
 *     summary: Delete event by ID
 *     tags: [Events]
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: eventId
 *         schema:
 *           type: string
 *         required: true
 *         description: The event ID
 *     responses:
 *       200:
 *         description: Event deleted successfully
 *       404:
 *         description: Event not found
 *       500:
 *         description: Internal server error
 */
router.delete('/events/:eventId', authMiddleware, eventController.deleteEvent);

/**
 * @swagger
 * /events:
 *   get:
 *     summary: List all events
 *     tags: [Events]
 *     responses:
 *       200:
 *         description: List of events retrieved successfully
 *       500:
 *         description: Internal server error
 */
router.get('/events', eventController.listEvents);



/**
 * @swagger
 * /events/{eventId}/editable/me:
 *   post:
 *     summary: Check if an event is editable and mark the current editing user
 *     tags: [Events]
 *     parameters:
 *       - in: path
 *         name: eventId
 *         schema:
 *           type: string
 *         required: true
 *         description: The event ID
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               userId:
 *                 type: string
 *     responses:
 *       200:
 *         description: Event is editable
 *       409:
 *         description: Event is being edited by another user
 *       500:
 *         description: Internal server error
 */
router.post('/events/:eventId/editable/me', eventController.checkEditable);

/**
 * @swagger
 * /events/{eventId}/editable/release:
 *   post:
 *     summary: Release the edit lock on an event
 *     tags: [Events]
 *     parameters:
 *       - in: path
 *         name: eventId
 *         schema:
 *           type: string
 *         required: true
 *         description: The event ID
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               userId:
 *                 type: string
 *     responses:
 *       200:
 *         description: Edit released
 *       409:
 *         description: You are not the editing user
 *       500:
 *         description: Internal server error
 */
router.post('/events/:eventId/editable/release', eventController.releaseEdit);

/**
 * @swagger
 * /events/{eventId}/editable/maintain:
 *   post:
 *     summary: Maintain the edit lock on an event
 *     tags: [Events]
 *     parameters:
 *       - in: path
 *         name: eventId
 *         schema:
 *           type: string
 *         required: true
 *         description: The event ID
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               userId:
 *                 type: string
 *     responses:
 *       200:
 *         description: Edit maintained
 *       409:
 *         description: You are not the editing user
 *       500:
 *         description: Internal server error
 */
router.post('/events/:eventId/editable/maintain', eventController.maintainEdit);


export default router;
