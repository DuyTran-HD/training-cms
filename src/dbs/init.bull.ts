import Queue from 'bull';
import globalConfig from '../common/config/global.config';

class BullQueue {
  private static instance: BullQueue;
  private queue!: Queue.Queue<any>;

  private constructor() {
    this.connect();
  }

  private async connect(): Promise<void> {
    try {
      const REDIS_URL = globalConfig.db.redis.url;
      this.queue = new Queue('jobQueue', REDIS_URL);

      await this.queue.isReady();
      console.log('Bull queue connected to Redis');

      this.queue.on('error', (error) => {
        console.error('Bull queue connection error:', error);
      
      });
    } catch (error) {
      console.error('Error connecting to Bull queue:', error);
     
    }
  }

  public static getInstance(): BullQueue {
    if (!BullQueue.instance) {
      BullQueue.instance = new BullQueue();
    }
    return BullQueue.instance;
  }

  public getQueue() {
    return this.queue;
  }
}
const instanceBullQueue = BullQueue.getInstance();
export default instanceBullQueue;


