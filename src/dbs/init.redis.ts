import { createClient } from 'redis';
import globalConfig from '../common/config/global.config';

class RedisDatabase {
  private static instance: RedisDatabase;
  private client!: ReturnType<typeof createClient>;

  private constructor() {
    this.connect();
  }

  private async connect(): Promise<void> {
    try {
      const REDIS_URL = globalConfig.db.redis.url;
      this.client = createClient({ url: REDIS_URL });

      this.client.on('error', (err) => console.error('Redis Client Error', err));

      await this.client.connect();
      // console.log('Connected to Redis');
    } catch (error) {
      console.error('Error connecting to Redis:', error);
      setTimeout(this.connect.bind(this), 5000); 
    }
  }

  public static getInstance(): RedisDatabase {
    if (!RedisDatabase.instance) {
      RedisDatabase.instance = new RedisDatabase();
    }
    return RedisDatabase.instance;
  }

  public getClient() {
    return this.client;
  }
}

const instanceRedis = RedisDatabase.getInstance();
export default instanceRedis;
