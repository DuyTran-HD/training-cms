import mongoose from 'mongoose';
import Agenda from 'agenda';
import globalConfig from '../common/config/global.config';
class Database {
  private static instance: Database;
  private agenda!: Agenda;

  private constructor() {
    this.connect();
  }

  private async connect(type: string = 'mongodb'): Promise<void> {
    if (type === 'mongodb') {
      mongoose.set('debug', true);
      mongoose.set('debug', { color: true });
      const connectString: string = globalConfig.db.mongo.url; 
      try {
        await mongoose.connect(connectString);
        console.log('Connected to MongoDB');

        this.agenda = new Agenda({ db: { address: connectString } });

        this.agenda.define('checkDatabaseConnection', async (job: any) => {
          try {
            await mongoose.connection.db.command({ ping: 1 });
            console.log('Database connection is stable.');
          } catch (error) {
            console.error('Error checking database connection:', error);
          }
        });

        await this.agenda.start();
        console.log('Agenda has started.');

        this.agenda.every('30 seconds', 'checkDatabaseConnection');
      } catch (error) {
        console.error('Error connecting to the database:', error);
        process.exit(1);
      }
    } else {
      console.error(`Unsupported database type: ${type}`);
    }
  }

  public static getInstance(): Database {
    if (!Database.instance) {
      Database.instance = new Database();
    }
    return Database.instance;
  }
}

const instanceMongodb = Database.getInstance();
export default instanceMongodb;
