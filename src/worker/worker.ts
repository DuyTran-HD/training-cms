import express, { Express } from "express";
import bodyParser from 'body-parser';
import instanceBullQueue from "../dbs/init.bull";
import './processor';
instanceBullQueue;
const app: Express = express();
app.use(bodyParser.json());
export default app;
