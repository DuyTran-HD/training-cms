import instanceBullQueue from '../dbs/init.bull';

const jobQueue = instanceBullQueue.getQueue();

jobQueue.process('sendEmail', async (job) => {
  try {
    console.log(`Processing job with id ${job.id}`);
    console.log('Job data:', job.data);

    await new Promise((resolve) => setTimeout(resolve, 1000)); 

    console.log(`Job with id ${job.id} has been processed`);
  } catch (error) {
    console.error('Error processing job:', error);
    throw error;
  }
});

jobQueue.on('completed', (job) => {
  console.log(`Job with id ${job.id} has been completed and removed`);
});

jobQueue.on('failed', (job, err) => {
  console.log(`Job with id ${job.id} has failed with error ${err.message}`);
});
