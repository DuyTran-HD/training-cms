import Queue from "bull";
import EventEmitter from "events";
import { log } from "console";

import instanceBullQueue from "../dbs/init.bull";
import { sendEmailData } from "../jobs/data.job";
class EmailEventEmitter extends EventEmitter {}
const eventEmitter = new EmailEventEmitter();

eventEmitter.on("addToQueue", (jobData: sendEmailData) => {
  try {
    instanceBullQueue.getQueue().add("sendEmail", jobData);
  } catch (error) {
    log(error);
  }
});


export default eventEmitter;
